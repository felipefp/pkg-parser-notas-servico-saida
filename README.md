# Introdução

Esse módulo Go define interfaces (notasaida/) que todas as implementações de leitura de notas emitidas de saída devem seguir, além de implementações reais para alguns municípios.

# Municípios implementados

Hoje temos as seguintes implementações:

* São Paulo (lê arquivo CSV contendo informações de múltiplas notas fiscais)
* Cabreúva (lê arquivo XML contendo múltiplas tags NotaFiscal)
* Rio Negrinho (lê pasta contendo múltiplos XMLs individuais)

# Como contribuir com este repositório

Após realizar as alterações desejadas, crie uma nova tag utilizando versão semântica, e envie a nova tag para o repositório remoto (Bitbucket).

É importante entender a versão semântica para não quebrar clientes deste módulo. Em caso de dúvidas, peça ajuda.

Comandos que podem te ajudar:

`git tag <VERSAO_SEMANTICA>`

`git push origin <VERSAO_SEMANTICA>`

Em seguida, atualize também os clientes (usuários deste módulo) que serão beneficiados pela nova versão:

Atualize a versão no arquivo go.mod do cliente, e limpe registros de cache do arquivo go.sum do cliente. No próximo run, build ou test, a nova versão será baixada e utilizada.

# Como consumir esse pacote

Para exemplo de implementação de cliente real, ver repositório tnh-nfs-xml-to-json-sped.

Para exemplo simplificado, ver testes deste repositório ou trecho abaixo:

```
package main

import (
    "fmt"

    "bitbucket.org/foxsuporte/pkg-parser-notas-servico-saida/cabreuva"
    //"bitbucket.org/foxsuporte/pkg-parser-notas-servico-saida/notasaida"
    //"bitbucket.org/foxsuporte/pkg-parser-notas-servico-saida/rionegrinho"
    //"bitbucket.org/foxsuporte/pkg-parser-notas-servico-saida/saopaulo"
)

func main() {
    // Você pode alterar "cabreuva" abaixo para "rionegrinho" ou "saopaulo",
    // e a utilização (API) é a mesma, já que todos implementam a interface "notasaida"
    lote, err := cabreuva.NewLoteNotaSaida("/caminho/para/arquivo/xml/cabreuva.xml")
    if err != nil {
        panic(err)
    }

    notas, err := lote.ListarTodas()
    if err != nil {
        panic(err)
    }

    for _, nota := range notas {
        fmt.Println("Data emissão:", nota.DataEmissao())
        fmt.Println("Nome tomador:", nota.TomadorNomeRazaoSocial())
        fmt.Println("Valor:", nota.ValorTotal())
    }
}
```
